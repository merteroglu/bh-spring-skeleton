package com.blueharvest.bankaccount.repository;

import com.blueharvest.bankaccount.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
