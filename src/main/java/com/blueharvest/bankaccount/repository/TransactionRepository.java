package com.blueharvest.bankaccount.repository;

import com.blueharvest.bankaccount.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {
}
