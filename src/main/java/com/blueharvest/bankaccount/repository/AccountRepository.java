package com.blueharvest.bankaccount.repository;

import com.blueharvest.bankaccount.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Long> {
}
